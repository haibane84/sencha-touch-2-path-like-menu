/**
 * Controller: Menu
 *
 */
Ext.define('PathMenu.controller.Menu', {
	extend: 'Ext.app.Controller',
	requires: [
		'PathMenu.view.MenuItemButton',
		'PathMenu.view.MenuButton'
	],

	config: {
		views: [
		],

		stores: [
		],

		models: [
		],

		refs: {
			menubutton: 'menubutton',
			menuitembutton: 'menuitembutton'
		},

		control: {
			menubutton: {
				tap: 'onMenuButtonTap'
			},
			menuitembutton: {
				tap: 'onMenuItemButtonTap'
			}
		}
	},

	/**
	 * Controller Init
	 */
	init: function () {

	},

	/**
	 * Controller Launch
	 *
	 * @param {} application
	 */
	launch: function(app) {
		Ext.Viewport.add([
			{
				xtype: 'menuitembutton',
				iconCls: 'action',
				cardIndex: 0
			},
			{
				xtype: 'menuitembutton',
				iconCls: 'add',
				cardIndex: 1
			},
			{
				xtype: 'menuitembutton',
				iconCls: 'compose',
				cardIndex: 2
			},
			{
				xtype: 'menuitembutton',
				iconCls: 'home',
				cardIndex: 3
			},
			{
				xtype: 'menuitembutton',
				iconCls: 'refresh',
				cardIndex: 4
			},
			{
				xtype: 'menubutton'
			}

		]);
	},

	/**
	 * [onMenuItemButtonTap description]
	 * @param  {[type]} button [description]
	 * @return {[type]}        [description]
	 */
	onMenuItemButtonTap: function(button) {
		var	menuButton = Ext.ComponentQuery.query('menubutton')[0];
		button.addCls('tapped');
		this.closeMenu(menuButton);


		this.getApplication().getController('Application').showView(button.getCardIndex(),{
			type: 'slide',
			direction: 'left',
			duration: 300
		});
	},

	/**
	 * [onMenuButtonTap description]
	 * 
	 * @param  {[type]} menuButton [description]
	 * @return {[type]}            [description]
	 */
	onMenuButtonTap: function(menuButton) {
		if (!menuButton.getIsOpen()) {
			// open menu
			this.openMenu(menuButton);
		} else {
			// close Menu
			this.closeMenu(menuButton);

		}
	},

	/**
	 * [openMenu description]
	 * 
	 * @param  {[type]} menuButton [description]
	 * @return {[type]}            [description]
	 */
	openMenu: function(menuButton) {
		var	items = Ext.ComponentQuery.query('menuitembutton'),
			bottom = menuButton.getBottom(),
			left = menuButton.getLeft(),
			radius = 150,
			section = items.length - 1
			angle = 90 / section;

		menuButton.replaceCls('close', 'open');

		Ext.each(items, function(item, index) {
			item.addCls('menuitembutton');
			item.replaceCls('close', 'open');

			var currentAngle = (90 - (angle * (section - index))),
				radiant = Math.PI / 180,
				currnetRadiant = radiant * currentAngle,
				x = Math.round(Math.cos(currnetRadiant) * radius),
				y = Math.round(Math.sin(currnetRadiant) * radius);

			item.setLeft(left + x);
			item.setBottom(bottom + y);
		});

		menuButton.setIsOpen(true);
	},

	/**
	 * [closeMenu description]
	 * 
	 * @param  {[type]} menuButton [description]
	 * @return {[type]}            [description]
	 */
	closeMenu: function(menuButton) {
		var	items = Ext.ComponentQuery.query('menuitembutton');

		menuButton.replaceCls('open', 'close');

		Ext.each(items, function(item, index) {
			if (item.getCls().indexOf('tapped') === -1) {
				item.replaceCls('open', 'close');
				item.setLeft(10);
				item.setBottom(10);
			} else {

				var task = Ext.create(
					'Ext.util.DelayedTask',
					function() {
						item.removeCls('menuitembutton');
						item.setLeft(10);
						item.setBottom(10);
						item.removeCls('tapped');
					}
				);
				task.delay(900);

			}
		});
		menuButton.setIsOpen(false);
	}
});