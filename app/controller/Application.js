Ext.define('Path.controller.Application', {
	extend: 'Ext.app.Controller',

	config: {
		views: [
			'Viewport'
		],


		refs: {
			mainView: 'mainview'

		},
		control: {

		}
	},

	// called when the Application is launched, remove if not needed
	launch: function(app) {
		this.initViewport();
	},

	/**
	 *
	 */
	initViewport: function() {
		var viewport = Ext.create('Path.view.Viewport');

		// Initialize the main view
		Ext.Viewport.setActiveItem(viewport);
	},

	/**
	 * Main View add handler
	 *
	 * @param {} viewObj
	 * @param {} animationObj
	 */
	showView: function (viewObj, animationObj) {
		var mainView = this.getMainView();
		mainView.animateActiveItem(viewObj, animationObj);
	}
});