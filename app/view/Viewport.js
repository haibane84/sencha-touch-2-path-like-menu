Ext.define('Path.view.Viewport', {
	extend: 'Ext.Container',
	xtype: 'view-port',
	requires: [
		'Path.view.MainView'
	],

	config: {
		layout: 'fit',
		items: [
			{
				xtype: 'mainview'
			}
		]
	}
});