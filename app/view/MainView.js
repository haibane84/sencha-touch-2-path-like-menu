Ext.define('Path.view.MainView', {
	extend: 'Ext.Container',
	alias: 'widget.mainview',
	xtype: 'mainview',

	config: {
		layout: 'card',
		defaults: {
			cls: 'bg',
			scrollable: true,
			styleHtmlContent: true,
			html: 'A path like menu for Sencha Touch 2. <br><br>' +
				'<b>git repo:</b> https://bitbucket.org/nilsdehl/<br>' +
				'<b>website:</b> http://nils-dehl.de<br>' +
				'<b>twitter:</b> @nilsdehl<br>' + 
				'<b>forum:</b> mrsunshine<br>'
		},
		items: [
			{
				items: [
					{
						xtype: 'titlebar',
						title: 'Path like Menu'
					}
				]
			},
			{
				items: [
					{
						xtype: 'titlebar',
						title: 'Path like Menu 2'
					}
				]
			},
			{
				cls: 'dummy yellow',
				items: [
					{
						xtype: 'titlebar',
						title: 'Path like Menu 4'
					}
				]
			},
			{
				items: [
					{
						xtype: 'titlebar',
						title: 'Path like Menu 4'
					}
				]
			},
			{
				items: [
					{
						xtype: 'titlebar',
						title: 'Path like Menu 5'
					}
				]
			}
		]
	}
});